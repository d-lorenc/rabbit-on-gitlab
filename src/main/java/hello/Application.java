package hello;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableRabbit
public class Application {

    static final String queueName = "spring-boot";

    @Bean
    Queue queue() {
        return new Queue(queueName, true);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args).close();
    }
}
